bundle (2.31-2) UNRELEASED; urgency=low

  * Add Bugs control field.
  * Conflict with the RubyGems "bundler" package, which also contains a
    /usr/bin/bundle program.

 -- Russ Allbery <rra@debian.org>  Mon, 06 Oct 2008 15:14:32 -0700

bundle (2.31-1) unstable; urgency=low

  * New upstream release.
    - Fix reporting of chmod and chown of directories.
    - Don't run diff when updating directories.

 -- Russ Allbery <rra@debian.org>  Sun, 05 Oct 2008 21:28:57 -0700

bundle (2.30-1) unstable; urgency=low

  * New upstream release.
  * Update debhelper compatibility level to V7.
    - Use some dh rule minimization.
    - Use dh_install and dh_installman instead of calling install.
  * Move Homepage to a real control field.
  * Add Vcs-Git and Vcs-Browser control fields.
  * Rewrite debian/copyright in the new proposed format.
  * Fix get-orig-source to not overwrite an existing bundle.
  * Remove version on perl build dependency satisfied by oldstable.
  * Declare the get-orig-source target PHONY.
  * Add a comments-only watch file explaining why watch isn't useful.
  * Update standards version to 3.8.0 (no changes required).

 -- Russ Allbery <rra@debian.org>  Mon, 29 Sep 2008 23:12:33 -0700

bundle (2.29-1) unstable; urgency=low

  * New upstream release.
    - Adds -C option to change directories before running bundle.
  * Rename build-orig to get-orig-source.
  * Remove stamp files first thing in the clean rule.
  * Add build-arch and build-indep, just in case.
  * Reorganize debian/rules.
  * Update debian/copyright to my current format.
  * Follow best practice for a homepage link in the long description.
  * Update to standards version 3.6.2 (no changes required).
  * Update maintainer address.

 -- Russ Allbery <rra@debian.org>  Fri, 31 Mar 2006 16:40:26 -0800

bundle (2.27-1) unstable; urgency=low

  * Initial release.

 -- Russ Allbery <rra@stanford.edu>  Sun, 13 Mar 2005 14:16:32 -0800

